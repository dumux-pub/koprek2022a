SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

A. Koprek <br>
Thermodynamic Analysis of Carbon Dioxide Mass Transport in a Stagnant Water Column<br>
Bachelor's Thesis, 2022<br>
Universität Stuttgart

Installation
============

The easiest way to install this module is to create a new directory and download `install_koprek2022a.py` from this repository.

```
mkdir koprek2022a && cd koprek2022a
python3 install_koprek2022a.py
```

This should automatically download all necessary modules and check out the correct versions. Afterwards dunecontrol is run.



After the script has run successfully, you may build all executables

```bash
cd DUMUX/koprek2022a/build-cmake
make build_tests
```
