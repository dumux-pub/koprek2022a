#!/usr/bin/env python3

# 
# This installs the module koprek2022a and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |      module name      |      branch name      |                 commit sha                 |         commit date         |
# |-----------------------|-----------------------|--------------------------------------------|-----------------------------|
# |      dune-common      |  origin/releases/2.8  |  9666e3783ecc20b4801c0dfe04022388113e8e38  |  2022-03-07 18:26:07 +0000  |
# |     dune-geometry     |  origin/releases/2.8  |  e7bfb66e48496aa28e47974c33ea9a4579bf723b  |  2021-08-31 17:51:20 +0000  |
# |       dune-grid       |  origin/releases/2.8  |  046d99d0f31f77b8ae3270ee571609da601c81d7  |  2022-03-07 13:46:18 +0000  |
# |  dune-localfunctions  |  origin/releases/2.8  |  5785450d1e79bcbf34c2a36fb89ac43a4123dcb8  |  2021-11-25 15:10:40 +0000  |
# |       dune-istl       |  origin/releases/2.8  |  3f387842cedeb35d1a0429f18c36a1570519d1b7  |  2021-11-25 15:10:07 +0000  |
# |     dune-foamgrid     |     origin/master     |  43bfdb6181fae187fd803eca935a030d8d5ab0bc  |  2021-07-03 20:20:08 +0000  |
# |      dune-alugrid     |  origin/releases/2.8  |  ffd6cd15dc7092593c3d5827c2a2b43b81d7a32f  |  2021-08-22 11:59:22 +0200  |
# |      dune-subgrid     |  origin/releases/2.8  |  2f8a21a7df84fe01eedb5680868f68c8356d6bd9  |  2021-09-21 12:26:46 +0200  |
# |         dumux         |     origin/master     |  c0098a92a849e85b7ee59a51b19a02ae592d03d4  |  2022-03-02 08:05:38 +0000  |
# |      koprek2022a      |      origin/main      |  f0191aa5118801e78dde31cbec12cd23512fd1ea  |  2022-07-12 13:11:42 +0000  |

import os
import sys
import subprocess

top = "DUMUX"
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/releases/2.8", "9666e3783ecc20b4801c0dfe04022388113e8e38", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/releases/2.8", "e7bfb66e48496aa28e47974c33ea9a4579bf723b", )

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/releases/2.8", "046d99d0f31f77b8ae3270ee571609da601c81d7", )

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/releases/2.8", "5785450d1e79bcbf34c2a36fb89ac43a4123dcb8", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/releases/2.8", "3f387842cedeb35d1a0429f18c36a1570519d1b7", )

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid.git", "origin/master", "43bfdb6181fae187fd803eca935a030d8d5ab0bc", )

print("Installing dune-alugrid")
installModule("dune-alugrid", "https://gitlab.dune-project.org/extensions/dune-alugrid.git", "origin/releases/2.8", "ffd6cd15dc7092593c3d5827c2a2b43b81d7a32f", )

print("Installing dune-subgrid")
installModule("dune-subgrid", "https://gitlab.dune-project.org/extensions/dune-subgrid", "origin/releases/2.8", "2f8a21a7df84fe01eedb5680868f68c8356d6bd9", )

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "origin/master", "c0098a92a849e85b7ee59a51b19a02ae592d03d4", )

print("Installing koprek2022a")
installModule("koprek2022a", "https://git.iws.uni-stuttgart.de/dumux-pub/koprek2022a", "origin/main", "f0191aa5118801e78dde31cbec12cd23512fd1ea", )

print("Applying patch for uncommitted changes in dumux")
patch = """
diff --git a/dumux/material/binarycoefficients/brine_co2.hh b/dumux/material/binarycoefficients/brine_co2.hh
index 2872ad637..0f5e4de28 100644
--- a/dumux/material/binarycoefficients/brine_co2.hh
+++ b/dumux/material/binarycoefficients/brine_co2.hh
@@ -83,14 +83,9 @@ public:
     static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
     {
         //Diffusion coefficient of CO2 in the brine phase
-        static const bool hasLiquidDiffCoeff = hasParam(\"BinaryCoefficients.LiquidDiffCoeff\");
-        if (!hasLiquidDiffCoeff) //in case one might set that user-specific as e.g. in dumux-lecture/mm/convectivemixing
-            return 2e-9;
-        else
-        {
-            const Scalar D = getParam<Scalar>(\"BinaryCoefficients.LiquidDiffCoeff\");
-            return D;
-        }
+        const Scalar Texp = 273 + 25; // [K] see poling et al p. 665   
+        const Scalar Dexp = 2.0e-9; // [m^2/s]                          
+        return Dexp * temperature/Texp;
     }
 
     /*!
"""
applyPatch("dumux", patch)

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=dumux/cmake.opts', 'all'],
    '.'
)
